package com.wirecard.payment.usecases;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wirecard.payment.domain.entity.Payment;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class SavePaymentTest {
	
	@InjectMocks
	private SavePayment savePaymentInjects;
	
	@Mock
	private SavePayment savePayment;
	
	private Payment payment = new Payment();
	
	@Test
	public void save_payment_transacion() {
		savePayment.savePaymentTransacion(payment);
	}
	
	@Test
	public void find_payment_transacion() {
		
		lenient().when(savePayment.findPayment("123456789")).thenReturn(payment);
		Payment response = savePaymentInjects.findPayment("123456789");
		
		assertEquals(response, null);
		
	}
}
