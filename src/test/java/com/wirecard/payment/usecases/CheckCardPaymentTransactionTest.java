package com.wirecard.payment.usecases;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.usecases.port.CreditCardClient;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class CheckCardPaymentTransactionTest {
	
	@InjectMocks
	private CheckCardPaymentTransaction cardPaymentInjects;
	
	@Mock
	private CreditCardClient cardClient;

	@Test
	public void check_payment_transaction_is_allowed_true() {
		
		Payment payment = new Payment();
		payment.setAmount(500.00);
		
		lenient().when(cardClient.checkPaymentAllowed(payment)).thenReturn(true);
		boolean response = cardPaymentInjects.checkPaymentTransactionIsAllowed(payment);
		
		assertEquals(response, true);
	}
	
	@Test
	public void check_payment_transaction_is_allowed_false() {
		
		Payment payment = new Payment();
		payment.setAmount(10.00);
		
		lenient().when(cardClient.checkPaymentAllowed(payment)).thenReturn(false);
		boolean response = cardPaymentInjects.checkPaymentTransactionIsAllowed(payment);
		
		assertEquals(response, false);
	}
}
