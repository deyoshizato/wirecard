package com.wirecard.payment.domain.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class CardTest {

	@Test
	public void card_get_and_set_test() {
		Card card = new Card();
		card.setCvv(1);
		card.setExpirationDate("01/22");
		card.setHolderName("Guilherme");
		card.setNumber(1L);
		
		assertEquals(1, card.getCvv());
		assertEquals("01/22", card.getExpirationDate());
		assertEquals("Guilherme", card.getHolderName());
		assertEquals(1L, card.getNumber());
	}
}
