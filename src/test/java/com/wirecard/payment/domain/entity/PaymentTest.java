package com.wirecard.payment.domain.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class PaymentTest {

	@Test
	public void payment_get_and_set_test() {
		Payment payment = new Payment();
		Card card = new Card();
		payment.setAmount(1.0);
		payment.setCard(card);
		payment.setNumberBoleto(1L);
		payment.setPaymentType(PaymentType.BOLETO);
		payment.setStatusPayment(StatusPayment.FAILURE);
		
		assertEquals(1.0, payment.getAmount());
		assertEquals(card, payment.getCard());
		assertEquals(1L, payment.getNumberBoleto());
		assertEquals(PaymentType.BOLETO, payment.getPaymentType());
		assertEquals(StatusPayment.FAILURE, payment.getStatusPayment());
	}
}
