package com.wirecard.payment.domain.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class ClientTest {
	
	@Test
	public void client_get_and_set_test() {
		Client client = new Client();
		client.setId(1L);
		
		assertEquals(1L, client.getId());
	}
}
