package com.wirecard.payment.adapter.creditcard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wirecard.payment.domain.entity.Payment;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class CreditCardClientImplementTest {

	@InjectMocks
	private CreditCardClientImplement cardClientImplementInjects;

	@Mock
	private CreditCardClientImplement cardClientImplement;

	@Test
	public void check_payment_allowed_success() {
		Payment payment = new Payment();
		payment.setAmount(100.00);
		lenient().when(cardClientImplement.checkPaymentAllowed(payment)).thenReturn(true);
		boolean response = cardClientImplementInjects.checkPaymentAllowed(payment);
		
		assertEquals(true, response);
	}
	
	@Test
	public void check_payment_allowed_fail() {
		Payment payment = new Payment();
		payment.setAmount(10.00);
		lenient().when(cardClientImplement.checkPaymentAllowed(payment)).thenReturn(false);
		boolean response = cardClientImplementInjects.checkPaymentAllowed(payment);
		
		assertEquals(false, response);
	}
}
