package com.wirecard.payment.adapter.webservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.entity.PaymentType;
import com.wirecard.payment.domain.entity.StatusPayment;
import com.wirecard.payment.usecases.CheckCardPaymentTransaction;
import com.wirecard.payment.usecases.SavePayment;
import com.wirecard.payment.usecases.ValidatePaymentMethod;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class PaymentControllerTest {

	@InjectMocks
	private PaymentController paymentController;

	@Mock
	private ValidatePaymentMethod validatePayment;

	@Mock
	private CheckCardPaymentTransaction cardTransaction;

	@Mock
	private SavePayment savePayment;

	@Test
	public void payment_by_card_transaction_allowed() {
		Payment payment = preparePaymentByCardObject();
		lenient().when(validatePayment.isCardPaymentMethod(payment)).thenReturn(true);
		lenient().when(cardTransaction.checkPaymentTransactionIsAllowed(payment)).thenReturn(true);
		lenient().doNothing().when(savePayment).savePaymentTransacion(payment);
		
		ResponseEntity<String> response = paymentController.paymentByBoletoAndCard(payment);
		
		assertEquals(new ResponseEntity<String>(StatusPayment.SUCCESS.getStatus(), HttpStatus.CREATED), response);
	}
	
	@Test
	public void payment_by_card_transaction_not_allowed() {
		Payment payment = preparePaymentByCardObject();
		lenient().when(validatePayment.isCardPaymentMethod(payment)).thenReturn(true);
		lenient().when(cardTransaction.checkPaymentTransactionIsAllowed(payment)).thenReturn(false);
		lenient().doNothing().when(savePayment).savePaymentTransacion(payment);
		
		ResponseEntity<String> response = paymentController.paymentByBoletoAndCard(payment);
		
		assertEquals(new ResponseEntity<String>(StatusPayment.FAILURE.getStatus(), HttpStatus.UNPROCESSABLE_ENTITY), response);
	}
	
	@Test
	public void payment_by_boleto_transaction_success() {
		Payment payment = preparePaymentByBoletoObject();
		lenient().when(validatePayment.isBoletoPaymentMethod(payment)).thenReturn(true);
		lenient().doNothing().when(savePayment).savePaymentTransacion(payment);
		
		ResponseEntity<String> response = paymentController.paymentByBoletoAndCard(payment);
		
		assertEquals(new ResponseEntity<String>(StatusPayment.BOLETONUMBER.getStatus(), HttpStatus.CREATED), response);
	}
	
	@Test
	public void payment_by_boleto_transaction_fail() {
		Payment payment = preparePaymentByBoletoObject();
		lenient().when(validatePayment.isBoletoPaymentMethod(payment)).thenReturn(false);
		lenient().doNothing().when(savePayment).savePaymentTransacion(payment);
		
		ResponseEntity<String> response = paymentController.paymentByBoletoAndCard(payment);
		
		assertEquals(new ResponseEntity<String>(StatusPayment.FAILURE.getStatus(), HttpStatus.UNPROCESSABLE_ENTITY), response);
	}
	
	private Payment preparePaymentByCardObject() {
		Payment payment = new Payment();
		payment.setPaymentType(PaymentType.CARD);
		return payment;
	}
	
	private Payment preparePaymentByBoletoObject() {
		Payment payment = new Payment();
		payment.setPaymentType(PaymentType.BOLETO);
		return payment;
	}
}
