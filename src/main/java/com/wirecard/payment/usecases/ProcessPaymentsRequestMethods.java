package com.wirecard.payment.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.entity.StatusPayment;
import com.wirecard.payment.domain.exception.WireCardException;
import com.wirecard.payment.usecases.port.PaymentRepository;

@Service
public class ProcessPaymentsRequestMethods {

	@Autowired
	ValidatePaymentMethod validate;

	@Autowired
	CheckCardPaymentTransaction cardTransaction;

	@Autowired
	PaymentRepository repository;
	
	public Payment findPayment(String id) {
		return repository.findById(id);
	}

	public Boolean process(Payment payment) throws WireCardException {
		if (validate.isCardPaymentMethod(payment)) {
			return creditCartPaymentProcess(payment);
		}
		return processBoletoPaymentMethod(payment);
	}

	private Boolean processBoletoPaymentMethod(Payment payment) throws WireCardException {
		payment.setStatusPayment(StatusPayment.SUCCESS);
		repository.save(payment);
		return true;
	}

	private Boolean creditCartPaymentProcess(Payment payment) throws WireCardException {
		if (cardTransaction.checkPaymentTransactionIsAllowed(payment)) {
			payment.setStatusPayment(StatusPayment.SUCCESS);
			repository.save(payment);
			return true;
		}
		payment.setStatusPayment(StatusPayment.FAILURE);
		repository.save(payment);
		return false;
	}
}
