package com.wirecard.payment.usecases.port;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.exception.WireCardException;

public interface PaymentRepository {

	void save(Payment payment) throws WireCardException;
	Payment findById(String id);
}
