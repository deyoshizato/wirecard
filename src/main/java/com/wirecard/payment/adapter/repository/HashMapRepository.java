package com.wirecard.payment.adapter.repository;

import java.util.HashMap;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.exception.WireCardException;
import com.wirecard.payment.usecases.port.PaymentRepository;

public class HashMapRepository implements PaymentRepository {

	private HashMap<String, Payment> payments = new HashMap<>();

	private static final Logger LOGGER = LoggerFactory.getLogger(HashMapRepository.class);

	@Override
	public void save(Payment payment) throws WireCardException {
		UUID idPayment = UUID.randomUUID();
		payments.put(idPayment.toString(), payment);
		LOGGER.info("Payment ID: " + idPayment.toString());
	}

	@Override
	public Payment findById(String id) {
		if (payments.containsKey(id)) 
			return payments.get(id);
		return null;
	}
}
