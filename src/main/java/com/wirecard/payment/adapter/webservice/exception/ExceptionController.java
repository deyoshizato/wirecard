package com.wirecard.payment.adapter.webservice.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.wirecard.payment.domain.exception.ExceptionDetails;
import com.wirecard.payment.domain.exception.WireCardException;
import com.wirecard.payment.usecases.util.Utils;

@ControllerAdvice
public class ExceptionController{

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleExceptionInternal(Exception ex, 
			HttpHeaders headers, 
			HttpStatus status) {
		ExceptionDetails errors = ExceptionDetails.Builder
				.newBuilder()
				.timestamp(Utils.dateFormat())
				.status(HttpStatus.NOT_FOUND.value())
				.title("Erro na chamada do banco de dados")
				.details("Erro na chamada do banco de dados")
				.message(ex.getClass().getName())
				.build();
		return new ResponseEntity<>(errors, headers, status);
	}

	@ExceptionHandler(WireCardException.class)
	public ResponseEntity<Object> wireCardHandler(Exception ex,
			HttpHeaders headers, 
			HttpStatus status){
		ExceptionDetails errors = ExceptionDetails.Builder
				.newBuilder()
				.timestamp(Utils.dateFormat())
				.status(HttpStatus.NOT_FOUND.value())
				.title("Erro na chamada do banco de dados")
				.details("Erro na chamada do banco de dados")
				.message(ex.getClass().getName())
				.build();
		return new ResponseEntity<>(errors, headers, status);
	}
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<Object> illegalStateException(Exception ex,
			HttpHeaders headers, 
			HttpStatus status){
		ExceptionDetails errors = ExceptionDetails.Builder
				.newBuilder()
				.timestamp(Utils.dateFormat())
				.status(HttpStatus.NOT_FOUND.value())
				.title("Erro na chamada do banco de dados")
				.details("Erro na chamada do banco de dados")
				.message(ex.getClass().getName())
				.build();
		return new ResponseEntity<>(errors, headers, status);
	}
	
}