package com.wirecard.payment.adapter.webservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.entity.StatusPayment;
import com.wirecard.payment.usecases.ProcessPaymentsRequestMethods;

@RestController
@RequestMapping("/wirecard")
public class PaymentController {

	@Autowired
	ProcessPaymentsRequestMethods paymentProcess;

	@PostMapping(path = "/payments")
	public ResponseEntity<String> requestPayment(@RequestBody Payment payment) {
		if (paymentProcess.process(payment))
			new ResponseEntity<String>(StatusPayment.SUCCESS.getStatus(), HttpStatus.CREATED);
		return new ResponseEntity<String>(StatusPayment.FAILURE.getStatus(), HttpStatus.UNPROCESSABLE_ENTITY);
	}

	@GetMapping(path = "/payments/{id_payment}/status")
	public ResponseEntity<Payment> paymentStatus(@PathVariable(value = "id_payment") String id) {
		Payment response = paymentProcess.findPayment(id);
		if (response != null)
			return new ResponseEntity<>(response, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}
