package com.wirecard.payment.adapter.creditcard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.usecases.port.CreditCardClient;

@Component
public class CreditCardClientImplement implements CreditCardClient {
	
	private static final Double value = 100.00;

	private static final Logger LOGGER = LoggerFactory.getLogger(CreditCardClientImplement.class);
	@Override
	public Boolean checkPaymentAllowed(Payment payment) {
		LOGGER.debug("checking payment is allowed.");
		return (payment.getAmount() >= value);
	}
}
