package com.wirecard.payment.domain.entity;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"status", "amount","type", "boleto", "card" })
public class Payment {
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("amount")
	private Double amount;
	
	@JsonProperty("type")
	private PaymentType paymentType;
	
	@JsonProperty("boleto")
	private Long numberBoleto;
	
	@JsonProperty("card")
	private Card card;
	
	@JsonProperty("status")
	private StatusPayment statusPayment;
	
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public Long getNumberBoleto() {
		return numberBoleto;
	}

	public void setNumberBoleto(Long numberBoleto) {
		this.numberBoleto = numberBoleto;
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}
	
	public StatusPayment getStatusPayment() {
		return statusPayment;
	}

	public void setStatusPayment(StatusPayment statusPayment) {
		this.statusPayment = statusPayment;
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Payment [amount=" + amount + ", paymentType=" + paymentType + ", numberBoleto=" + numberBoleto
				+ ", card=" + card + ", statusPayment=" + statusPayment + "]";
	}
}