package com.wirecard.payment.domain.entity;


public enum PaymentType {
	
	BOLETO("boleto"),
	CARD("cartao");
	
	private String type;

	private PaymentType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}