package com.wirecard.payment.domain.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Client {
	
	@JsonProperty("id")
	private Long id;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Client [id=" + id + "]";
	}
}
