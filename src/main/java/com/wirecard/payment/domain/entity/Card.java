package com.wirecard.payment.domain.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Card {
	
	@JsonProperty("holder_name")
	private String holderName;
	
	@JsonProperty("number")
	private Long number;
	
	@JsonProperty("expiration_date")
	private String expirationDate;
	
	@JsonProperty("verification_code")
	private Integer cvv;
	
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public Long getNumber() {
		return number;
	}
	public void setNumber(Long number) {
		this.number = number;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public Integer getCvv() {
		return cvv;
	}
	public void setCvv(Integer cvv) {
		this.cvv = cvv;
	}
}
