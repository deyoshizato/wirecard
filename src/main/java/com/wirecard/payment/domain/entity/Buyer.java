package com.wirecard.payment.domain.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class Buyer {
	
	@JsonProperty("client")
	private Client client;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("email")
	private String email;
	
	@JsonProperty("cpf")
	private Long cpf;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getCpf() {
		return cpf;
	}
	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	@Override
	public String toString() {
		return "Buyer [name=" + name + ", email=" + email + ", cpf=" + cpf + "]";
	}
}
