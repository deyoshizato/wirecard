package com.wirecard.payment.domain.exception;

public class WireCardException extends RuntimeException {

	private static final long serialVersionUID = -2912140840205511927L;

	public WireCardException() {
    }

    public WireCardException(final String message) {
        super(message);
    }

    public WireCardException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public WireCardException(final Throwable cause) {
        super(cause);
    }

    public WireCardException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
