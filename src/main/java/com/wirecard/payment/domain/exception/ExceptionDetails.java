package com.wirecard.payment.domain.exception;

public class ExceptionDetails {

	private String title;
	private int status;
	private String detail;
	private String timestamp;
	private String message;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public static final class Builder {
		
		private String title;
		private int status;
		private String details;
		private String timestamp;
		private String message;
		
		private Builder() {
		}
		public static Builder newBuilder() {
			return new Builder();
		}
		public Builder title(String title) {
			this.title = title;
			return this;
		}
		public Builder status(int status) {
			this.status = status;
			return this;
		}
		public Builder details(String details) {
			this.details = details;
			return this;
		}
		public Builder timestamp(String timestamp) {
			this.timestamp = timestamp;
			return this;
		}
		public Builder message(String message) {
			this.message = message;
			return this;
		}
		public ExceptionDetails build() {
			ExceptionDetails errors = new ExceptionDetails();
			errors.setTitle(title);
			errors.setStatus(status);
			errors.setDetail(details);
			errors.setTimestamp(timestamp);
			errors.setMessage(message);
			return errors;
		}
	}
}